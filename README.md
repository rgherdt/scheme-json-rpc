# An implementation of the JSON-RPC for Scheme


## Introduction

*EXPERIMENTAL*

The JSON-RPC allows calling procedures on remote servers by
exchanging JSON objects. It's specification can be found at
[https://www.jsonrpc.org/specification](https://www.jsonrpc.org/specification).

It currently supports CHICKEN 5, Gambit >=4.9.4 and Guile 3.

## Installing

### CHICKEN
This library is available as an .egg. Install it by running:
```
chicken-install -s json-rpc
```

### Gambit

Required Gambit version: 4.9.4

You can install the library using Gambit's built-in package manager by running this:

```
gsi -install codeberg.org/rgherdt/srfi/srfi/145 \
             github.com/ashinn/irregex/irregex \
             codeberg.org/rgherdt/scheme-json-rpc/json-rpc
```

### Guile
You can install the library for Guile executing following commands in the `guile`
subfolder:

```
cd guile/
./configure
make
sudo make install
```

## Documentation

### High-level API

#### [syntax] (define-notification-handler (handler-name method params) body ...)

Define and install a notification handler `handler-name` for `method`.

According to LSP, notifications are used for their side effects and the return value is ignored.
Alternatively you can use the `json-rpc-handler-table` parameter directly (see below).


#### [syntax] (define-request-handler (handler-name method params) body ...)

Define and install a request handler `handler-name` for `method`. Example:

```scheme
(define-request-handler (subtract-handler "subtract" params)
    (- (vector-ref params 0)
       (vector-ref params 1)))
```

Alternatively you can use the `json-rpc-handler-table` parameter directly (see below).


#### [procedure] (json-rpc-call method params #!optional in-port out-port)
#### [procedure] (json-rpc-call/tcp method params tcp-address tcp-port)

Call the remote procedure `method` (string) with the JSON-mappable scheme object `params`. See [srfi 180](https://srfi.schemers.org/srfi-180/srfi-180.html).
If `in-port` and `out-port` are omitted, their values default to `current-input-port` and `current-output-port` respectively.

#### [procedure] (json-rpc-send-notification method params #!optional out-port)
Call the remote procedure `method` (string) with the JSON-mappable scheme object `params`. If `out-port` is omitted,
`(current-output-port)` is used instead.


#### [procedure] (json-rpc-start-server/tcp tcp-port)

Listen on `tcp-port` and loop over input connections, answering requests according to installed handlers.

#### [parameter] (json-rpc-handler-table)

An association list mapping method names (string) to functions that get an (unmarshalled) scheme object as input. These handlers must return one of the following:
- a scheme object that is 'mappable' to a JSON string, which becomes the `result` content of the response. See [srfi 180](https://srfi.schemers.org/srfi-180/srfi-180.html) for information about mapping between JSON and Scheme objects.
- `#f`, if no response is expected. This is the way to handle notifications.


### Low-level API

#### [procedure] (json-rpc-loop input-port output-port)

JSON-RPC 2.0 demands that all requests are answered. This functions loops over `input-port` and answers to the requests through `output-port` using the handlers defined in `json-rpc-handler-table` (or defined via the `define-` macros described before).

### Error handling

User-created errors are meant to be raised by request handlers, and are properly delivered to the calling process according to the JSON-RPC protocol.

#### [parameter] (custom-error-codes)

JSON-RPC supports custom server-side error codes. This parameter defines an alist mapping custom error names (symbol) to error codes (integer) ranging between -32000 and -32099.

#### [procedure] (make-json-rpc-custom-error error-symbol [msg])

Create an error instance of `error-symbol` (symbol) with an optional `msg` (string). It's an error if `error-type` isn't defined in `custom-error-codes`.

#### [procedure] (make-json-rpc-internal-error)

Create a generic error object.

#### [procedure] (json-rpc-error? condition)
#### [procedure] (json-rpc-custom-error? condition)
#### [procedure] (json-rpc-internal-error? condition)

Predicates for several error objects.

### Examples

```scheme
;; server.scm
(import (json-rpc))

(define-request-handler (hello-handler "hello" params)
  (let ((name (cdr (assoc 'name params))))
    (string-append "Hello " name)))

(json-rpc-start-server/tcp 4242)

;; client.scm
(import (json-rpc))
(json-rpc-call/tcp "hello" '((name . "World!")) "127.0.0.1" 4242)
```

For more examples, see `tests/`.

## LICENSE

MIT License

Copyright (c) 2021 Ricardo G. Herdt

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
