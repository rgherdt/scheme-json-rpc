(define-library (json-rpc lolevel)

(export json-rpc-handler-table
        json-rpc-log-level
        json-rpc-loop
        json-rpc-read
        json-rpc-write
        json-rpc-version
        json-rpc-exit

        custom-error-codes
        make-json-rpc-custom-error
        make-json-rpc-internal-error
        make-json-rpc-invalid-request-error
        json-rpc-error?
        json-rpc-custom-error?
        json-rpc-invalid-request-error?
        json-rpc-internal-error?
        json-rpc-parse-error?)

(import (scheme char)
        (json-rpc private)
        (json-rpc private compat)
        (only (srfi 1) filter)
        (srfi 28)
        (srfi 69))

(cond-expand
 (chicken
  (import (except (scheme base)
                  string-length string-ref string-set! make-string string substring
                  string->list list->string string-fill! write-char read-char write-string)
          (only (chicken base)
                alist-ref)
          (only (chicken port)
                with-output-to-string)
          (only (chicken condition)
                print-error-message)
          (only (utf8-srfi-13)
                string-take
                string-trim
                string-trim-right)
          (only utf8 display string-length read-char)))
 (gambit
  (import (except (scheme base) guard)
          (scheme write)
          (rename (only (gambit)
                        r7rs-guard
                        with-output-to-file)
                  (r7rs-guard guard))
          (only (srfi 13)
                string-take
                string-trim
                string-trim-right)))
 (guile
  (import (only (scheme base)
                (scheme write)
                define-record-type
                eof-object
                flush-output-port
                guard
                read-bytevector
                read-error?
                read-line
                textual-port?
                utf8->string
                vector-for-each
                vector-map)
          (scheme char)
          (only (scheme inexact) infinite?)
          (srfi 13))))

(include "lolevel-impl.scm")
)
