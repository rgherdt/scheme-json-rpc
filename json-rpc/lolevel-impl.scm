(define json-rpc-version "2.0")

(define core-error-codes
  '((parse-error . -32700)
    (invalid-request . -32600)
    (method-not-found . -32601)
    (invalid-parameters . -32602)
    (internal-error . -32603)))

(define custom-error-codes
  (make-parameter '()))

(define last-continuation
  (make-parameter #f))

(define (error-codes)
  (append core-error-codes (custom-error-codes)))

(define error-code-to-symbol-map
  (map (lambda (p)
         (cons (cdr p) (car p)))
       (error-codes)))

(define (symbol->error-code symb)
  (let ((code (assoc symb (error-codes))))
    (if (not code)
        (write-log 'error (format "symbol->error-code: invalid symbol ~s"
                                  symb))
        (cdr code))))

(define (json-rpc-loop in out)
  (let loop ((state 'read)
             (message #f))
    (write-log 'debug
               (format "loop (state ~s) (message ~s)~%"
                       state message))
    (case state
      ((read)
       (let ((res (json-rpc-read in)))
         (cond ((not res)
                res)
               ((json-rpc-parse-error? res)
                (loop 'write (make-json-rpc-parse-error)))
               (else
                (loop 'eval res)))))
      ((write)
       (json-rpc-write message out)
       (loop 'read #f))
      ((eval)
       (call/cc
        (lambda (k)
          (set! last-continuation k)))
       (let ((res (json-rpc-compute-response message)))
         (write-log 'debug
                    (format "computed response: ~s~%"
                            res))
         (cond ((not res)
                ;; notification
                (loop 'read #f))
               ((json-rpc-error? res)
                (loop 'write res))
               ((and (pair? res)
                     (json-rpc-exit? (cdr (assoc 'result res))))
                (loop 'stop json-rpc-exit))
               (else (loop 'write res)))))
      ((stop)
       #f)
      (else (error "invalid state" state)))))

(define json-rpc-handler-table
  (make-parameter '() (lambda (alist)
                        (if (null? alist)
                            ;; due to a bug in Gambit 4.9.4, alist->hash-table
                            ;; over the empty list crashes. Fixed on master.
                            (make-hash-table)
                            (alist->hash-table alist equal?)))))

(define (consume-header in-port)
  (let loop ((c (peek-char in-port))
             (lines '()))
    (cond ((eof-object? c)
           (read-char in-port)
           c)
          ((or (char=? c #\newline)
               (char=? c #\return))
           (read-char in-port)
           (loop (peek-char in-port)
                 lines))
          ((char=? c #\C)
           (let ((line (read-line in-port)))
             (loop (peek-char in-port)
                   (append lines (list line)))))
          (else
           (apply string-append lines)))))

(define (consume-remaining in-port)
  (if (char-ready? in-port)
      (let loop ((c (read-char in-port)))
        (cond ((eof-object? c)
               (eof-object))
              ((not (char-ready? in-port))
               #t)
              (else
               (loop (read-char in-port)))))
      #t))

(define (json-rpc-read in-port)
  (define header (consume-header in-port))
  (write-log 'debug
             (truncate-string
              (format "received header: ~s~%"
                      header)))
  (cond ((eof-object? header)
         #f)
        ((or (not header) (null? header)) #f)
        (else
         (guard
             (condition
              (else
               (write-log 'debug
                          (format "json-rpc-read: json-error: ~s~%" condition))
               (consume-remaining in-port)
               (make-json-rpc-parse-error)))
           (json-read in-port)))))

(define (json-rpc-resume)
  (let ((k (last-continuation)))
    (if k
        (k #t)
        #f)))

(define (vector-remove-false vec)
  (list->vector (filter (lambda (x) x) (vector->list vec))))

(define (json-rpc-write scm . args)
  (define str ((scheme->json-string) scm))
  (define port
    (if (not (null? args))
        (car args)
        (current-output-port)))
  (write-log 'debug
             (format "json-rpc-write: ~s~%"
                     str))
  (write-string-standard (format "Content-Length: ~a\r\n\r\n"
                                 (+ 4 (string-length str)))
                         port)
  (write-string-standard str port)
  (write-string-standard "\r\n" port)
  (write-string-standard "\r\n" port)
  (flush-output-port port))

(define (json-rpc-compute-response request)
  (define (process req)
    (if (list? req)
        (guard
            (condition
             ((json-rpc-error? condition)
              (write-log 'debug (format "~s" condition))
              condition)
             ((json-rpc-exit? condition)
              #f))
          (json-rpc-dispatch req))
        (make-json-rpc-invalid-request-error)))
  (cond ((or (not request) (null? request))
         #f)
        ((json-rpc-exit? request)
         'json-rpc-exit)
        ((vector? request)
         (if (= (vector-length request) 0)
             (make-json-rpc-invalid-request-error '((message . "Invalid Request")))
             (let ((res (vector-remove-false (vector-map process request))))
               (if (= (vector-length res) 0)
                   #f
                   res))))
        (else
         (process request))))

(define (json-rpc-dispatch request)
  (define (dispatch j)
    (let* ((method-pair (assoc 'method j))
           (params-pair (assoc 'params j)))
      (cond (method-pair
             (let ((method-name (cdr method-pair)))
               (cond ((and (not (pair? method-name))
                           (string? method-name))
                      (let* ((method (hash-table-ref/default
                                      (json-rpc-handler-table)
                                      method-name
                                      #f)))
                        (cond ((not method)
                               (make-json-rpc-method-not-found-error
                                `((message . ,method-name))))
                              (params-pair
                               (let ((params (extract-params params-pair)))
                                 (method params)))
                              (else (method)))))
                     (else
                      (write-log 'debug
                                 (format "json-rpc-dispatch: invalid method name ~s~%"
                                         method-name))
                      (make-json-rpc-invalid-request-error)))))
            (else 
             (write-log 'debug (format "json-rpc-dispatch: missing method: ~s~%"
                                       j))
             (make-json-rpc-invalid-request-error)))))
  (cond ((not request)
         #f)
        (else
         (let ((id (assoc 'id request))
               (result (dispatch request)))
           (cond ((not result) ;; notification
                  #f)
                 (id (if (json-rpc-error? result)
                         (cons id result)
                         (make-response (cdr id) result)))
                 ((json-rpc-error? result)
                  result)
                 (result
                  (make-response #f result))
                 (else
                  (make-json-rpc-invalid-request-error)))))))

(define json-rpc-exit 'json-rpc-exit)

(define (json-rpc-exit? req)
  (eq? req 'json-rpc-exit))

(define (make-response id result)
  (if id
      `((jsonrpc . ,json-rpc-version)
        (id . ,id)
        (result . ,result))
      `((jsonrpc . ,json-rpc-version)
        (id . null)
        (result . ,result))))

(define (extract-params params-pair)
  (cdr params-pair))

(define-syntax define-json-rpc-error
  (syntax-rules ()
    ((define-json-rpc-error ctor pred error-symbol msg)
     (begin
       (define (ctor . args)
         (let* ((message (assoc 'message args))
                (id (assoc 'id args))
                (res `((json-rpc . ,json-rpc-version)
                       (error . ((message . ,(if message
                                                 (cdr message)
                                                 'null))
                                 (code . ,(alist-ref error-symbol
                                                     (error-codes))))))))
           (if id
               (cons `(id . ,(cdr id)) res)
               res)))
       (define (pred condition)
         (and (pair? condition)
              (let ((error-pair (assoc 'error condition)))
                (and error-pair
                     (let ((code-pair (assoc 'code (cdr error-pair))))
                       (and code-pair
                            (member (cdr code-pair)
                                    (map cdr (error-codes)))))))))))))

(define-json-rpc-error
  make-json-rpc-internal-error
  json-rpc-internal-error?
  'internal-error
  "Internal error")

(define-json-rpc-error
  make-json-rpc-parse-error
  json-rpc-parse-error?
  'parse-error
  "Parse error")

(define-json-rpc-error
  make-json-rpc-invalid-request-error
  json-rpc-invalid-request-error?
  'invalid-request
  "Invalid request error")

(define-json-rpc-error
  make-json-rpc-method-not-found-error
  json-rpc-method-not-found-error?
  'method-not-found
  "Method not found")

(define (make-json-rpc-custom-error error-symbol msg)
  `((json-rpc . ,json-rpc-version)
    (error . `((message . ,msg)
               (code . ,(alist-ref error-symbol (custom-error-codes)))))))

(define (json-rpc-custom-error? condition)
  (and (pair? condition)
       (let ((error-pair (assoc 'error condition)))
         (and error-pair
              (let ((code-pair (assoc 'code (cdr error-pair))))
                (and code-pair
                     (member (cdr code-pair)
                             (map cdr (error-codes)))))))))

(define (json-rpc-error? condition)
  (and (pair? condition)
       (let ((error-pair (assoc 'error condition)))
         (and error-pair
              (let ((code-pair (assoc 'code (cdr error-pair))))
                (and code-pair
                     (member (cdr code-pair)
                             (map cdr (error-codes)))))))))

(define (json-rpc-error-contents err)
  (cdr err))

(define (truncate-string str)
  (define max-length 150)
  (if (< (string-length str) max-length)
      str
      (string-append (string-take str max-length) " ...")))



