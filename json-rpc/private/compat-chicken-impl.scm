(define scheme->json-string
  (make-parameter (lambda (scm)
                    (call-with-output-string
                     (lambda (p) (json-write scm p))))))

(define (write-string-standard str #!optional port)
  (utf8#write-string str #f port))
