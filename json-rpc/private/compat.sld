(define-library (json-rpc private compat)

(export alist-ref
        call-with-output-string
        json-read
        scheme->json-string
        with-output-to-string
        tcp-read-timeout
        tcp-accept
        tcp-connect
        tcp-close
        tcp-listen
        write-string-standard)

(import (scheme case-lambda)
        (scheme file)
        (scheme write)
        (only (srfi 13) string-upcase)
        (srfi 28))

(cond-expand
 (chicken (import (r7rs)
                  (only (chicken base)
                        alist-ref)
                  (only (chicken port)
                        call-with-output-string
                        with-output-to-string)
                  (chicken tcp)
                  (srfi 180))
          (include "compat-chicken-impl.scm"))
 (guile (import (guile)
                (only (scheme base)
                      define-record-type
                      eof-object
                      flush-output-port
                      guard
                      read-bytevector
                      read-error?
                      read-line
                      textual-port?
                      utf8->string
                      vector-for-each
                      write-string)
                (only (srfi 60) arithmetic-shift bitwise-ior)
                (only (scheme inexact) infinite?))
        (include "compat-guile-impl.scm"))
 (gambit (import (except (gambit)
                         string-upcase)
                 (scheme case-lambda)
                 (only (scheme inexact) infinite? nan?)
                 (srfi 145)
                 (srfi 180))
         (include "compat-gambit-impl.scm"))
 (else)))
