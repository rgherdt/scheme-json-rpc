(define (alist-ref key alist)
    (define match (assoc key alist))
    (if match
        (cdr match)
        #f))

(define json-string->scheme
  (make-parameter
   (lambda (j)
     (call-with-input-string j
                             (lambda (p)
                               (json-read p))))))

(define scheme->json-string
  (make-parameter (lambda (scm)
                    (call-with-output-string
                     (lambda (p) (json-write scm p))))))

(define (tcp-accept listener)
  (define res (accept listener))
  (define port (car res))
  (values port port))

(define (tcp-close conn)
  (close conn))

(define (tcp-connect tcp-address tcp-port-number)
  (define sock (socket PF_INET SOCK_STREAM 0))
  (define addr (inet-pton AF_INET tcp-address))
  (setsockopt sock SOL_SOCKET SO_REUSEADDR 1)
  (connect sock (make-socket-address AF_INET addr tcp-port-number))
  (values sock sock))

(define (tcp-listen port-number)
  (define sock (socket PF_INET SOCK_STREAM 0))
  (setsockopt sock SOL_SOCKET SO_REUSEADDR 1)
  (bind sock (make-socket-address AF_INET INADDR_LOOPBACK port-number))
  (listen sock 20)
  sock)

;; ignored for now
(define tcp-read-timeout (make-parameter #f))

(define write-string-standard write-string)

