(define-library (json-rpc private)

(export json-rpc-log-file
        json-rpc-log-level
        satisfies-log-level?
        write-log)

(import (scheme base)
        (scheme case-lambda)
        (scheme file)
        (srfi 28))

(cond-expand (chicken (import utf8
                              (utf8-srfi-13)))
             (guile (import (only (guile) open-file)
                            (scheme write)
                            (only (srfi 13) string-upcase)))
             (else (import (scheme write)
                           (only (srfi 13) string-upcase))))

(include "private-impl.scm")
)
