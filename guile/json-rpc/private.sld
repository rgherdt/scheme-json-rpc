(define-module (json-rpc private)

#:export (json-rpc-log-file
          json-rpc-log-level
          satisfies-log-level?
          write-log)

#:use-module ((scheme base) #:select (flush-output-port))
#:use-module (scheme case-lambda)
#:use-module (scheme file)
#:use-module (scheme write)
#:use-module ((srfi srfi-13) #:select (string-upcase))
#:use-module (srfi srfi-28)
#:use-module ((guile) #:select (open-file))

#:declarative? #f)
