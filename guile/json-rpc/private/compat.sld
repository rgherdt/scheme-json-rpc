(define-module (json-rpc private compat)

#:export (alist-ref
          json-error?
          json-string->scheme
          scheme->json-string
          tcp-read-timeout
          tcp-accept
          tcp-connect
          tcp-close
          tcp-listen
          write-string-standard)

#:re-export (json-read)

#:use-module ((scheme base)
              #:select (define-record-type
                           call-with-port
                         eof-object
                         error-object?
                         flush-output-port
                         guard
                         read-bytevector
                         read-error?
                         read-line
                         textual-port?
                         utf8->string
                         vector-for-each
                         write-string))
#:use-module ((scheme inexact) #:select (infinite?))
#:use-module (scheme write)
#:use-module (srfi srfi-13)
#:use-module (srfi srfi-28)
#:use-module ((srfi srfi-60) #:select (arithmetic-shift bitwise-ior))
#:use-module (srfi srfi-145)
#:use-module (srfi srfi-180)

#:declarative? #f)

