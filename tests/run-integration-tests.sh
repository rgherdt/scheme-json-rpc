#!/bin/bash

CUR_DIR=`dirname "$0"`

CHICKEN_CMD="csi -qb"
GUILE_CMD="guile --no-auto-compile --r7rs"
GAMBIT_CMD="gsi"

echo "Running integration tests"
echo "======================================================="

echo "[CHICKEN]"
echo "starting server"
$CHICKEN_CMD $CUR_DIR/server.scm &
sleep 1

echo "starting client"
$CHICKEN_CMD $CUR_DIR/client.scm

pid=`ps aux | grep  csi | grep  "server.scm" | awk '{print $2}'`

if ! [ -z $pid ]; then
    kill -KILL $pid
fi

echo "-------------------------------------------------------"

echo "[GUILE]"
echo "starting server"
$GUILE_CMD $CUR_DIR/server.scm &
sleep 1

echo "starting client"
$GUILE_CMD $CUR_DIR/client.scm

pid=`ps aux | grep  guile | grep  "server.scm" | awk '{print $2}'`

if ! [ -z $pid ]; then
    kill -KILL $pid
fi

echo "-------------------------------------------------------"

echo "[GAMBIT]"
echo "starting server"
$GAMBIT_CMD $CUR_DIR/server.scm &
sleep 1

echo "starting client"
$GAMBIT_CMD $CUR_DIR/client.scm

pid=`ps aux | grep gsi | grep  "server.scm" | awk '{print $2}'`

if ! [ -z $pid ]; then
    kill -KILL $pid
fi

echo "-------------------------------------------------------"

