(define-module-alias (chibi irregex)
  (github.com/ashinn/irregex))

(define-module-alias (srfi 145)
  (codeberg.org/rgherdt/srfi srfi 145))

(define-module-alias (srfi 180)
  (codeberg.org/rgherdt/srfi srfi 180))

(define-module-alias (json-rpc private)
  (codeberg.org/rgherdt/scheme-json-rpc json-rpc private))

(define-module-alias (json-rpc private compat)
  (codeberg.org/rgherdt/scheme-json-rpc json-rpc private compat))

(define-module-alias (json-rpc lolevel)
  (codeberg.org/rgherdt/scheme-json-rpc json-rpc lolevel))

(define-module-alias (json-rpc)
  (codeberg.org/rgherdt/scheme-json-rpc json-rpc))
