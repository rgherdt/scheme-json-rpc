(import (scheme base)

        (json-rpc)
        (json-rpc lolevel)
        (json-rpc private compat)
        (srfi 1)
        (srfi 28))

(cond-expand
 (chicken (import (utf8)
                  (srfi 18)
                  (srfi 64)))
 (gambit (import (_test)))
 (else (import (srfi 18)
               (srfi 64))))

(define-values (in out)
  (tcp-connect "127.0.0.1" 4220))

(test-begin "Integration tests")

(define (write-content content)
  (write-string-standard (format "Content-Length: ~a\r\n\r\n~a"
                                 (+ 4 (string-length content))
                                 content) out)
  (flush-output-port out))

(define (read-error-code in)
  (let* ((res (json-rpc-read in))
         (error-pair (assoc 'error res))
         (code (cdr (assoc 'code (cdr error-pair)))))
    code))

(define (get-error-code res)
  (let* ((error-pair (assoc 'error res))
         (code (cdr (assoc 'code (cdr error-pair)))))
    code))

(define (get-result res)
  (cdr (assoc 'result res)))

(define invalid-request-code -32600)
(define parse-error-code -32700)
(define method-not-found-code -32601)


(parameterize ((json-rpc-log-level 'error))
  (write-content "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"hello\", \"params\": {\"name\": \"World!\"}}")
  (let ((res (json-rpc-read in)))
    (test-equal "Hello World!" (cdr (assoc 'result res))))

  (write-content "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"grüß\", \"params\": {\"name\": \"Gott!\"}}")
  (let ((res (json-rpc-read in)))
    (test-equal "Grüß Gott!" (cdr (assoc 'result res))))

  (let ((res (json-rpc-call "hello" '((name . "Mundo!")) in out)))
    (test-equal "Hello Mundo!" (cdr (assoc 'result res))))

  (write-content "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"foobar, \"params\": \"bar\", \"baz]")
  (test-equal parse-error-code (read-error-code in))

  (write-content "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": 1, \"params\": \"bar\"}")
  (test-equal invalid-request-code (read-error-code in))

  (write-content "[{\"jsonrpc\": \"2.0\", \"method\": \"sum\", \"params\": [1,2,4], \"id\": \"1\"}, {\"jsonrpc\": \"2.0\", \"method\"]")
  (test-equal parse-error-code (read-error-code in))

  (write-content "[]")
  (test-equal invalid-request-code (read-error-code in))

  (write-content "[1]")
  (let* ((res (json-rpc-read in))
         (first-obj (vector-ref res 0))
         (error-pair (assoc 'error first-obj))
         (code (cdr (assoc 'code (cdr error-pair)))))
    (test-equal 1 (vector-length res))
    (test-equal invalid-request-code code))

  (write-content "[1, 2, 3]")
  (let* ((res (json-rpc-read in))
         (error-codes (vector->list (vector-map get-error-code res))))
    (test-equal 3 (vector-length res))
    (test-assert (every (lambda (code) (= code invalid-request-code))
                        error-codes)))

  (write-content "[
        {\"jsonrpc\": \"2.0\", \"method\": \"sum\", \"params\": [1,2,4], \"id\": \"1\"},
        {\"jsonrpc\": \"2.0\", \"method\": \"notify_hello\", \"params\": [7]},
        {\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [42,23], \"id\": \"2\"},
        {\"foo\": \"boo\"},
        {\"jsonrpc\": \"2.0\", \"method\": \"foo.get\", \"params\": {\"name\": \"myself\"}, \"id\": \"5\"},
        {\"jsonrpc\": \"2.0\", \"method\": \"get_data\", \"id\": \"9\"} 
]")
  (let ((res (json-rpc-read in)))
    (test-equal 5 (vector-length res))
    (test-equal 7 (get-result (vector-ref res 0)))
    (test-equal 19 (get-result (vector-ref res 1)))
    (test-equal invalid-request-code (get-error-code (vector-ref res 2)))
    (test-equal method-not-found-code (get-error-code (vector-ref res 3)))
    (test-equal #("hello" 5) (get-result (vector-ref res 4)))
    )

  (write-content "[
        {\"jsonrpc\": \"2.0\", \"method\": \"notify_sum\", \"params\": [1,2,4]},
        {\"jsonrpc\": \"2.0\", \"method\": \"notify_hello\", \"params\": [7]}
]")

  (write-content "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"exit\", \"params\": {}}")

  )

(close-input-port in)
(close-output-port out)

(test-end)


