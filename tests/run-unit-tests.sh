#!/bin/bash

CUR_DIR=`dirname "$0"`

echo "Running unit tests"
echo "======================================================="

echo "[CHICKEN]"
csi -s $CUR_DIR/run.scm

echo "-------------------------------------------------------"

echo "[Guile]"
guile --no-auto-compile --r7rs $CUR_DIR/run.scm
echo "-------------------------------------------------------"

echo "[Gambit]"
gsi $CUR_DIR/run.scm


