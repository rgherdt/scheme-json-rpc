#!/bin/bash

CUR_DIR=`dirname "$0"`

$CUR_DIR/run-unit-tests.sh
$CUR_DIR/run-integration-tests.sh
