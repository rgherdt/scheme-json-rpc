(define-library (json-rpc)

(export json-rpc-call
        json-rpc-call/tcp
        json-rpc-log-file
        json-rpc-log-level
        json-rpc-exit
        json-rpc-handler-table
        json-rpc-delete-handler!
        json-rpc-install-handler!
        json-rpc-send-request
        json-rpc-send-notification
        json-rpc-start-server/tcp

        define-notification-handler
        define-request-handler)

(import (scheme case-lambda)
        (scheme char)
        (json-rpc private)
        (json-rpc private compat)
        (json-rpc lolevel)
        (srfi 28)
        (srfi 69))

(cond-expand
 (chicken (import (except (r7rs)
                          string-length string-ref string-set! make-string string substring
                          string->list list->string string-fill! write-char read-char)
                  (only (chicken condition) print-error-message)
                  (chicken tcp)

                  utf8))
 (gambit (import (except (scheme base) guard)
                 (rename (only (gambit) r7rs-guard)
                         (r7rs-guard guard))
                 (scheme write)))
 (guile (import (scheme base)
                (scheme write)))
 (else))

(include "json-rpc-impl.scm"))
